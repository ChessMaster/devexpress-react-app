import { combineReducers } from 'redux'

import {patientReducer} from "./patient/reducer";

export const rootReducer = combineReducers({
  patientReducer: patientReducer,
})

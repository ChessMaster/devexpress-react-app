import { SET_PATIENTS, SET_TARGET_PATIENT } from "./actions";

const initialState = {
  patients: [
    {
      ID: 1,
      Name: "Иван",
      Department: "Кардиология 1",
      Sex: "М",
      IsIVL: false,
    },
    {
      ID: 2,
      Name: "Лиза",
      Department: "ОР1",
      Sex: "Ж",
      IsIVL: true,
    },
    {
      ID: 3,
      Name: "Алексей",
      Department: "Кардиология 1",
      Sex: "М",
      IsIVL: true,
    },
    {
      ID: 4,
      Name: "Наталья",
      Department: "Кардиология 900",
      Sex: "Ж",
      IsIVL: true,
    },
  ],
  targetPatientId: null,
};

export const patientReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_PATIENTS:
      return { ...state, patients: action.payload };
    case SET_TARGET_PATIENT:
      return { ...state, targetPatientId: action.payload };
    default:
      return state;
  }
};

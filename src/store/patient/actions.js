export const SET_PATIENTS = "SET_PATIENTS";
export const SET_TARGET_PATIENT = "SET_TARGET_PATIENT";

export const setPatients = (patients) => {
  return {
    type: SET_PATIENTS,
    payload: patients
  }
}

export const setTargetPatient = (id) => {
  return {
    type: SET_TARGET_PATIENT,
    payload: id
  }
}

import styles from "./PatientForm.module.css";

import React from "react";
import PropTypes from "prop-types";
import Form, { SimpleItem } from "devextreme-react/form";
import { connect } from "react-redux";
import { setPatients } from "../../store/patient/actions";

const PatientForm = ({ targetPatientId, patients, setPatients }) => {
  const departments = [
    ...new Set(
      patients.map((el) => {
        return el.Department;
      })
    ),
  ];

  const patientInfo = patients.find((el) => el.ID === targetPatientId);

  const onChange = (e) => {
    const targetArrIndex = patients.findIndex(
      (el) => el.ID === targetPatientId
    );
    const targetField = e.dataField;
    const newPatientInfo = { ...patientInfo, [targetField]: e.value };
    const newPatientsArray = [...patients];
    newPatientsArray[targetArrIndex] = newPatientInfo;

    setPatients(newPatientsArray);
  };

  return (
    <>
      {targetPatientId && (
        <div>
          <h1>{patientInfo.Name}</h1>
          <Form
            formData={patientInfo}
            className={styles.form}
            onFieldDataChanged={onChange}
          >
            <SimpleItem dataField="Name" label={{ text: "ФИО" }} />
            <SimpleItem
              dataField="Department"
              editorType="dxSelectBox"
              editorOptions={{
                items: departments,
              }}
              label={{ text: "Отделение" }}
            />
            <SimpleItem
              dataField={"Sex"}
              editorType={"dxRadioGroup"}
              editorOptions={{ dataSource: ["М", "Ж"] }}
              label={{ text: "Пол" }}
            />
            <SimpleItem
              dataField={"IsIVL"}
              editorType={"dxCheckBox"}
              label={{ text: "На ИВЛ" }}
            />
          </Form>
        </div>
      )}
    </>
  );
};

PatientForm.propTypes = {
  targetPatientId: PropTypes.number,
  patients: PropTypes.array.isRequired,
  setPatients: PropTypes.func.isRequired,
};

const mstp = (state) => ({
  targetPatientId: state.patientReducer.targetPatientId,
  patients: state.patientReducer.patients,
});

export default connect(mstp, { setPatients })(PatientForm);

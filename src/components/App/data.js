export const peoples = [
  {
    ID: 1,
    Name: "Иван",
    Department: "Кардиология 1",
    Sex: 'М',
    IsIVL: true,
  },
  {
    ID: 2,
    Name: "Лиза",
    Department: "ОР1",
    Sex: 'Ж',
    IsIVL: true,
  },
  {
    ID: 3,
    Name: "Алексей",
    Department: "Кардиология 1",
    Sex: 'М',
    IsIVL: true,
  },
  {
    ID: 4,
    Name: "Наталья",
    Department: "Кардиология 900",
    Sex: 'Ж',
    IsIVL: true,
  },
];

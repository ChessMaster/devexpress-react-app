import React from "react";

import "./App.css";
import "devextreme/dist/css/dx.common.css";
import "devextreme/dist/css/dx.light.css";

import { peoples } from "./data";
import PatientForm from "../PatientForm/PatientForm";
import PatientsTable from "../PatientsTable/PatientsTable";

const App = () => (
  <div className={"App"}>
    <PatientsTable />
    <PatientForm patientInfo={peoples[0]} patients={peoples} />
  </div>
);

export default App;

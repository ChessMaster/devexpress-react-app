import React, { useRef, useState } from "react";
import PropTypes from "prop-types";
import { DataGrid, SelectBox } from "devextreme-react";
import { connect } from "react-redux";
import { setTargetPatient } from "../../store/patient/actions";

const columns = [
  { dataField: "Name", caption: "ФИО" },
  { dataField: "Department", caption: "Отделение" },
  { dataField: "Sex", caption: "Пол" },
  { dataField: "IsIVL", caption: "На ИВЛ" },
];

const PatientsTable = ({ patients, setTargetPatient }) => {
  const gridRef = useRef(null);

  const statuses = [
    "Все",
    ...new Set(
      patients.map((el) => {
        return el.Department;
      })
    ),
  ];

  const [filterStatus, setFilterStatus] = useState(statuses[0]);

  const onValueChanged = ({ value }) => {
    const dataGrid = gridRef.current.instance;

    if (value === "Все") {
      dataGrid.clearFilter();
    } else {
      dataGrid.filter(["Department", "=", value]);
    }

    setFilterStatus(value);
  };

  const onRowFocus = (e)=> {
    setTargetPatient(e.row.data.ID)
  }

  return (
    <div>
      <h1>Пациенты</h1>
      <div className="right-side">
        <SelectBox
          items={statuses}
          value={filterStatus}
          onValueChanged={onValueChanged}
        />
      </div>
      <DataGrid
        keyExpr={"Name"}
        ref={gridRef}
        dataSource={patients}
        defaultColumns={columns}
        showBorders={true}
        focusedRowEnabled={true}
        onFocusedRowChanged={onRowFocus}
      />
    </div>
  );
};

PatientsTable.propTypes = {
  patients: PropTypes.array.isRequired,
  setTargetPatient: PropTypes.func.isRequired,
};

const mstp = (state) => ({
  patients: state.patientReducer.patients,
});

export default connect(mstp, { setTargetPatient })(PatientsTable);
